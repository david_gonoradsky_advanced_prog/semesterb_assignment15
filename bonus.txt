insert into accounts(id, balance) values(4, 0);
begin transaction;
update accounts set balance=800 where id=4;
update accounts set balance=0 where id=2;
commit;
alter table accounts add column name text;
update accounts set name="Jesus" where id=1;
update accounts set name="Moses" where id=2;
update accounts set name="Yosi" where id=3;
update accounts set name="David" where id=4;
select name as "ClientName", balance as "ClientBalance" from accounts;
Yosi|1100
Moses|0
Jesus|-400
David|800